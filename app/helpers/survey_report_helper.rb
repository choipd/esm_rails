# encoding: UTF-8

module SurveyReportHelper
  require 'csv'
  def this_is_your_view_helper_method
    table = Array.new
    header = Array.new
    header << "ID"
    (1..@survey.questions.count).each do |q|
      header << "Q.%d" % [q]
    end
	  table << header
	  
    @grid.each_with_index do |x, xi|
      x.each_with_index do |y, yi|
        line = Array.new
        line << "%s %d-%d" % [@join_projects[xi].user.name, yi / @survey.repeatTime + 1, yi % @survey.repeatTime + 1 ]
        
        if y
          y.each do |a|
            line << a.user_answer
          end            
        else
          (1..@survey.questions.count).each do |q|
            line << "n/a"
          end
        end
        table << line
      end
    end
    
	  
	  CSV.generate(force_quotes: true) do |csv|
	    table.each do |l|
	      csv << l
	    end
    end
  end
  
end
