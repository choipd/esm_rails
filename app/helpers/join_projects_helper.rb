# encoding: utf-8
module JoinProjectsHelper
  def createPlan(params)
    print 'create plan'
    print params
  end

  def makeSchedule(join_project)
  # 시간을 randomly generate해주는 로직
    for survey in join_project.project.surveys
      for day in 0...survey.days
        timecounter = survey.begin_at
        for time in 0...survey.repeatTime
          sample = survey.interval_min+Random.rand(survey.interval_max - survey.interval_min);
          timecounter += sample.minutes;
          puts "%d일 %d회차 %d %02d:%02d" % [day + 1, time + 1, sample, timecounter.hour, timecounter.min]
          ss = SurveySchedule.new
          ss.user = join_project.user
          ss.survey = survey
          ss.d = day + 1
          ss.c = time + 1
          ss.t = timecounter
          ss.attend = false
          ss.begin_at = (join_project.project.start_date.to_time + 
            day.days + timecounter.hour.hours + timecounter.min.minutes).utc
          ss.push_sent = false
          ss.save
        end
      end
    end
  end
  # 
  # makeSchedule(9, 21, 120, 30, 5, 8)
  
end
