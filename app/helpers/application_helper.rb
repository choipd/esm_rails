# encoding: utf-8
module ApplicationHelper
  def sendAPNS(args)
    puts args
    @deviceTokens = DeviceToken.where(:user_id => args["target_user"])
    for token in @deviceTokens do
      n = Rapns::Notification.new
      n.device_token = token.token
      n.alert = args["message"]
      n.badge = 0
      n.expiry = 2.minutes.to_i
      n.attributes_for_device = {"sid" => args["survey_id"], 
        "ssi" => args["survey_schedule_id"], "d" => args["day"], "t" => args["times"], "expire" => args["expire_date"]}
      # n.deliver_after = 1.hour.from_now
      n.save!
      puts("보낸다~! #{token.token}")
    end
  end
  
  def sendToTarget()
    list = SurveySchedule.where(begin_at: DateTime.now..(DateTime.now + 1.minutes), push_sent: false)
    for schedule in list do
      puts("Survey #{schedule.survey_id} user #{schedule.user_id}")
      sendAPNS({"target_user" => schedule.user_id, 
        "message" => 
        "[%s] %d일차 %d회차 조사입니다." % [schedule.survey.project.title.truncate(10), schedule.d, schedule.c],
        "survey_id" => 
        schedule.survey_id, 
        "day" => 
        schedule.d, 
        "times" => 
        schedule.c, 
        "expire_date" => 
        (Time.now + 20.minutes).utc.iso8601,
        "survey_schedule_id" =>
        schedule.id})
      schedule.push_sent = true
      schedule.save
    end
  end
        
end
