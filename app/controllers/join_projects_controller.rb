include JoinProjectsHelper

class JoinProjectsController < ApplicationController
  prepend_before_filter :get_api_key
  before_filter :authenticate_user!
  
  private
  def get_api_key
    if api_key = params[:api_key].blank? && request.headers["X-API-KEY"]
      params[:api_key] = api_key
    end
  end

  public
    
  def create
    @join_project = JoinProject.new(params[:join_project])
    @join_project.user = current_user
    @join_project.project_id = params[:project_id]
    
    respond_to do |format|
      print params
      if @join_project.save
        @history = UserHistory.new()
        @history.user = current_user
        @history.action = "[" + @join_project.project.title + ']'
        @history.params = @join_project
        @history.viewlevel = 3
        @history.save
        format.json { render json: @join_project, status: :created, location: @join_project }
      else
        print '****** failed!!!'
        format.json { render json: @join_project.errors, status: :unprocessable_entity }
      end
    end
  end
  
  def event
    @join_project = JoinProject.find(params[:id])    

    case params[:event]
    when "accept"
      puts "!!!! accpeted!!!!"
      makeSchedule(@join_project)
    when "reject", "abandon"
      puts "!!!! rejected, abandoned!!!"
      for survey in @join_project.project.surveys
        SurveySchedule.delete_all(user_id: @join_project.user, survey_id: survey)
      end
    when "reset_schedule"
      for survey in @join_project.project.surveys
        SurveySchedule.delete_all(user_id: @join_project.user, survey_id: survey)
      end
      makeSchedule(@join_project)      
    end
    
    
    eval('@join_project.' + params[:event] + '!')
    
    respond_to do |format|
      format.html { redirect_to(@join_project.project) }
      format.json { render json: @join_project }
    end
  end
  
end
