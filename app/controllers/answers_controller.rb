class AnswersController < ApplicationController
  prepend_before_filter :get_api_key
  before_filter :authenticate_user!
  
  private
  def get_api_key
    if api_key = params[:api_key].blank? && request.headers["X-API-KEY"]
      params[:api_key] = api_key
    end
  end

  public
  def create    
    ActiveRecord::Base.transaction do
      @result = true
      @answers = ActiveSupport::JSON.decode(params[:answers])

      survey_schedule_id = params[:survey_schedule_id]
      
      if survey_schedule_id
        @aSchedule = SurveySchedule.find(survey_schedule_id)
        if !@answers.last[:take_survey_id]
          @take_survey = TakeSurvey.new()
          @take_survey.survey_id = @aSchedule.survey_id
          @take_survey.survey_schedule_id = @aSchedule.id
          @take_survey.user = current_user
          @take_survey.workflow_state = :complete
          @take_survey.save
        end
      end
      
      
      @answers.each do |answer|
        @answer = Answer.create(answer)
        @answer.take_survey_id = @take_survey.id if !@answer.take_survey_id && @take_survey
        
        if @answer.save
          @result = true
        else
          @result = false
          break
        end
      end
    end

    if @result and @aSchedule
      @aSchedule.attend = true;
      @aSchedule.save;
    end
    
    respond_to do |format|
      format.json { render json: {:result => @result}, status: :unprocessable_entity }
    end
        
  end
  
end
