class SurveyReportController < ApplicationController
  before_filter :authenticate_user!
  
  def show
    @survey = Survey.find(params[:id])
    @join_projects = JoinProject.where(project_id: @survey.project.id)
    
    rows, cols = @join_projects.count, @survey.days * @survey.repeatTime
    @grid = Array.new(rows) { Array.new{cols} }
    
    @join_projects.each_with_index do |member, i|
      @survey_schedules = SurveySchedule.where(survey_id: params[:id], user_id: member.user_id).order(:id)
      @survey_schedules.each_with_index do |schedule, j|
        if schedule.attend
          puts "schedule id = %d" % [schedule.id]
          take_survey = TakeSurvey.where(survey_schedule_id: schedule.id)
          answers = Answer.where(take_survey_id: take_survey).order(:id)
          @grid[i][j] = answers
        else
          @grid[i][j] = nil
        end
      end
    end

    if stale? etag: [@survey_schedules]
      respond_to do |format|
        if params[:format] == 'csv'
          generate_csv_headers("survey_report-#{Time.now.strftime("%Y%m%d")}") 
        end
        
        format.html # show.html.erb
        format.csv { render layout: false }
        # format.json { render json: @survey.to_json(:include => {:questions => 
        #   {:only => [:id, :title, :qtype, :type_param, :description], :include => {:examples => {:only => [:id, :title]}}}}) }
      end
    end    
  end
  
  def generate_csv_headers(filename)
    headers.merge!({
      'Cache-Control'             => 'must-revalidate, post-check=0, pre-check=0',
      'Content-Type'              => 'text/csv',
      'Content-Disposition'       => "attachment; filename=\"#{filename}\"",
      'Content-Transfer-Encoding' => 'binary'
    })
  end
  
  
end
