class UserHistoriesController < ApplicationController
  prepend_before_filter :get_api_key
  before_filter :authenticate_user!, :except => [:index, :show]
  
  private
  def get_api_key
    if api_key = params[:api_key].blank? && request.headers["X-API-KEY"]
      params[:api_key] = api_key
    end
  end
  
  public
  # my projects
  def mine
    @histories = UserHistory.where(:user_id => current_user)
    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @histories.to_json }
    end
    
  end
  
end
