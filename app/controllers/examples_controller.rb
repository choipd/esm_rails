class ExamplesController < ApplicationController
  before_filter :authenticate_user!, :except => [:index, :show]
    
  def index
    @examples = Example.where('question_id = ?', params[:question_id]).order(:display_no)
    @question = Question.find(params[:question_id])

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @examples }
    end
  end
  
  def new
    @question = Question.find(params[:question_id])
    @example = Example.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @example }
    end
  end
  
  def show
    @example = Example.find(params[:id])
    @question = @example.question

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @example }
    end
  end
  
  def create
    question_id = params[:example][:question_id]
    @question = Question.find(question_id)
    @example = @question.examples.create(params[:example])
    
    respond_to do |format|
      if @example.save
        format.html { redirect_to question_examples_path(@question), notice: 'Example was successfully created.' }
        format.json { render json: @example, status: :created, location: @example }
      else
        format.html { render action: "new" }
        format.json { render json: @example.errors, status: :unprocessable_entity }
      end
    end
  end
  
  def edit
    @example = Example.find(params[:id])
    @question = @example.question
  end
  
  def update
    @example = Example.find(params[:id])
    @question = @example.question

    respond_to do |format|
      if @example.update_attributes(params[:example])
        format.html { redirect_to question_example_path(@question, @example), notice: 'Example was successfully updated.' }
        format.json { head :ok }
      else
        format.html { render action: "edit" }
        format.json { render json: @example.errors, status: :unprocessable_entity }
      end
    end
  end

  def destroy
    @example = Example.find(params[:id])
    @question = @example.question
    
    
    if user_signed_in? and current_user == @example.user
      @example.destroy

      respond_to do |format|
        format.html { redirect_to question_examples_path(@question) }
        format.json { head :ok }
      end
    else
      respond_to do |format|
        format.html { head :forbidden }
        format.json { head :forbidden }
      end
    end
  end
end
