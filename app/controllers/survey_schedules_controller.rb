class SurveySchedulesController < ApplicationController
  before_filter :authenticate_user!
    
  def index
    @survey_schedules = SurveySchedule.where(user_id: current_user, survey_id: params[:survey_id]).order("created_at")

    respond_to do |format|
      format.json { render json: @survey_schedules }
    end
  end
end
