class SurveysController < ApplicationController
  
  before_filter :authenticate_user!, :except => [:index, :show]
    
  def index
    @surveys = Survey.where('project_id = ?', params[:project_id])
    @project = Project.find(params[:project_id])

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @surveys }
    end
  end
  
  def show
    @survey = Survey.find(params[:id])
    @project = @survey.project
    @questions = @survey.questions.order(:display_no)
    if stale? etag: [@survey, @project, @questions]
      respond_to do |format|
        format.html # show.html.erb
        format.json { render json: @survey.to_json(:include => {:questions => 
          {:only => [:id, :title, :qtype, :type_param, :description], :include => {:examples => {:only => [:id, :title]}}}}) }
      end
    end    
  end
  
  def new
    @project = Project.find(params[:project_id])
    @survey = Survey.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @survey }
    end
  end
  
  def create
    project_id = params[:survey][:project_id]
    @project = Project.find(project_id)
    @survey = @project.surveys.create(params[:survey])
    
    respond_to do |format|
      if @survey.save
        format.html { redirect_to project_survey_path(@project, @survey), notice: 'Survey was successfully created.' }
        format.json { render json: @survey, status: :created, location: @survey }
      else
        format.html { render action: "new" }
        format.json { render json: @survey.errors, status: :unprocessable_entity }
      end
    end
  end
  
  def edit
    @survey = Survey.find(params[:id])
    @project = @survey.project
  end
  
  def update
    @survey = Survey.find(params[:id])
    @project = @survey.project

    respond_to do |format|
      if @survey.update_attributes(params[:survey])
        format.html { redirect_to project_survey_path(@project, @survey), notice: 'Survey was successfully updated.' }
        format.json { head :ok }
      else
        format.html { render action: "edit" }
        format.json { render json: @survey.errors, status: :unprocessable_entity }
      end
    end
  end

  def destroy
    @survey = Survey.find(params[:id])
    @project = @survey.project
    
    if user_signed_in? and current_user == @project.user
      @survey.destroy

      respond_to do |format|
        format.html { redirect_to project_surveys_path(@project) }
        format.json { head :ok }
      end
    else
      respond_to do |format|
        format.html { head :forbidden }
        format.json { head :forbidden }
      end
    end
  end
end
