class TakeSurveysController < ApplicationController
  prepend_before_filter :get_api_key
  before_filter :authenticate_user!
  
  private
  def get_api_key
    if api_key = params[:api_key].blank? && request.headers["X-API-KEY"]
      params[:api_key] = api_key
    end
  end

  public
  def create
    @take_survey = TakeSurvey.new(params[:take_survey])
    @take_survey.user = current_user
    
    respond_to do |format|
      if @take_survey.save
        format.html { redirect_to project_survey_path(@survey.project, @survey), notice: 'TakeSurvey was successfully created.' }
        format.json { render json: @take_survey, status: :created, location: @take_survey }
      else
        format.html { render action: "new" }
        format.json { render json: @take_survey.errors, status: :unprocessable_entity }
      end
    end
  end
end
