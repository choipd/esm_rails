class ProjectsController < ApplicationController
  prepend_before_filter :get_api_key
  before_filter :authenticate_user!, :except => [:index]
  
  private
  def get_api_key
    if api_key = params[:api_key].blank? && request.headers["X-API-KEY"]
      params[:api_key] = api_key
    end
  end
  
  public
  def index
    
    case params[:section_name]
    when 'featured'
      @projects = Project.where("start_date > ?", Date.today)
    when 'latest'
      @projects = Project.where("start_date > ?", Date.today).order('projects.created_at desc').limit(20)
    when 'popular'
      @projects = Project.where("start_date > ?", Date.today)
    else
      @projects = Project.all
    end
          
    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @projects.to_json(:include => {:user => {:only => [:name, :email]} }) }
      # format.json { render json: @survey.to_json(:include => {:questions => 
      #   {:only => [:id, :title], :include => {:examples => {:only => [:id, :title]}}}}) }
    end
  end
  
  # my projects
  def mine
    # @projects = JoinProject.find(:all, :include => :project)

    case params[:status_filter]
    when 'in_review'
      @projects = JoinProject.where("user_id = ? AND workflow_state IN ('requested', 'in_review')", current_user).order("updated_at").reverse_order
    when 'accepted'
      @projects = JoinProject.where("user_id = ? AND workflow_state = 'accepted'", current_user).order("updated_at").reverse_order
    when 'finished'
      @projects = JoinProject.where("user_id = ? AND workflow_state = 'finished'", current_user).order("updated_at").reverse_order
    end
    
    
    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @projects.to_json(:include => :project) }
      # format.json { render json: @survey.to_json(:include => {:questions => 
      #   {:only => [:id, :title], :include => {:examples => {:only => [:id, :title]}}}}) }
    end
    
  end
  
  
  def show

    begin
      @project = Project.find(params[:id])
      
    rescue ActiveRecord::RecordNotFound
      render json: {:error => "Project Not Found"}
      return
    end

    @surveys = @project.surveys
    @join_projects = JoinProject.where(:project_id => params[:id], :user_id => current_user.id)
    @members = JoinProject.where(:project_id => params[:id])
    

    respond_to do |format|
      format.html # new.html.erb
      format.json do
        if @join_projects.any?
          render json: {:project => @project, :user => @project.user.as_json(:only => [:name, :email]),
          :join_project => @join_projects.first}
        else
          render json: {:project => @project, :user => @project.user.as_json(:only => [:name, :email]) }
        end  
      end
    end
  end
  
  def new
    @project = Project.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @project }
    end
  end
  
  def create
    @project = Project.new(params[:project])
    @project.user = current_user

    respond_to do |format|
      if @project.save
        format.html { redirect_to @project, notice: 'Project was successfully created.' }
        format.json { render json: @project, status: :created, location: @project }
      else
        format.html { render action: "new" }
        format.json { render json: @project.errors, status: :unprocessable_entity }
      end
    end
  end
  
  def edit
    @project = Project.find(params[:id])
  end
  
  def update
    @project = Project.find(params[:id])
    @project.user = current_user

    respond_to do |format|
      if @project.update_attributes(params[:project])
        format.html { redirect_to @project, notice: 'Project was successfully updated.' }
        format.json { head :ok }
      else
        format.html { render action: "edit" }
        format.json { render json: @project.errors, status: :unprocessable_entity }
      end
    end
  end

  def destroy
    
    @project = Project.find(params[:id])
    print current_user, @project.user
    if user_signed_in? and current_user == @project.user
      @project.destroy

      respond_to do |format|
        format.html { redirect_to projects_url }
        format.json { head :ok }
      end
    else
      respond_to do |format|
        format.html { head :forbidden }
        format.json { head :forbidden }
      end
    end
  end
  
end
