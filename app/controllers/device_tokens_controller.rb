# encoding: utf-8
class DeviceTokensController < ApplicationController
  before_filter :authenticate_user!, :except => [:index, :show]
    
  prepend_before_filter :get_api_key
  before_filter :authenticate_user!
  
  private
  def get_api_key
    if api_key = params[:api_key].blank? && request.headers["X-API-KEY"]
      params[:api_key] = api_key
    end
  end

  public

  
  def create
    # reset device tokens
    DeviceToken.delete_all(user_id: current_user.id)
    
    @device_token = DeviceToken.new(params[:device_token])
    @device_token.user_id = current_user.id
    @device_token.token = params[:token]

    if @device_token.save
      render json: @device_token, status: :created
    else
      render json: @device_token.errors, status: :unprocessable_entity
    end
  end
  
  def destroy
    @device_token = DeviceToken.find(params[:id])
    if @device_token.destroy
      respond_to do |format|
        format.json { head :ok }
      end
    else
      respond_to do |format|
        format.json { head :forbidden }
      end
    end
  end
  
end
