class QuestionsController < ApplicationController
  before_filter :authenticate_user!, :except => [:index, :show]
    
  def index
    @questions = Question.all(:conditions => {:survey_id => params[:survey_id]}, :order => :display_no)
    @survey = Survey.find(params[:survey_id])

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @questions }
    end
  end
  
  def new
    @survey = Survey.find(params[:survey_id])
    @question = Question.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @question }
    end
  end
  
  def show
    @question = Question.find(params[:id])
    @survey = @question.survey
    @examples = @question.examples.order(:display_no)

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @question }
    end
  end
  
  def create
    survey_id = params[:question][:survey_id]
    @survey = Survey.find(survey_id)
    @question = @survey.questions.create(params[:question])
    
    respond_to do |format|
      if @question.save
        format.html { redirect_to survey_question_path(@survey, @question), notice: 'Question was successfully created.' }
        format.json { render json: @question, status: :created, location: @question }
      else
        format.html { render action: "new" }
        format.json { render json: @question.errors, status: :unprocessable_entity }
      end
    end
  end
  
  def edit
    @question = Question.find(params[:id])
    @survey = @question.survey
  end
  
  def update
    @question = Question.find(params[:id])
    @survey = @question.survey

    respond_to do |format|
      if @question.update_attributes(params[:question])
        format.html { redirect_to survey_question_path(@survey, @question), notice: 'Question was successfully updated.' }
        format.json { head :ok }
      else
        format.html { render action: "edit" }
        format.json { render json: @question.errors, status: :unprocessable_entity }
      end
    end
  end

  def destroy
    @question = Question.find(params[:id])
    @survey = @question.survey
    
    
    if user_signed_in? and current_user == @question.user
      @question.destroy

      respond_to do |format|
        format.html { redirect_to survey_questions_path(@survey) }
        format.json { head :ok }
      end
    else
      respond_to do |format|
        format.html { head :forbidden }
        format.json { head :forbidden }
      end
    end
  end
end
