class SessionsController < Devise::SessionsController
  before_filter :remove_tokens, :only => [:destroy] 
  def create
    super
  end
  
  def destroy
    super
  end

  protected
    def remove_tokens
      puts "remove_tokens"
      # TODO: remove device tokens
      # DeviceToken.delete_all(user_id: current_user.id)
    end
  
end