class TakeSurvey < ActiveRecord::Base
  belongs_to :user
  belongs_to :survey
  belongs_to :survey_schedule
  
  # validates_uniqueness_of :user_id, :scope => :survey_id
  
  include Workflow
  workflow do
    state :before_taking do
      event :take, :transitions_to => :on_taking
    end
    state :on_taking do
      event :finish, :transitions_to => :done
      event :timeout, :transitions_to => :not_finish
    end
    state :complete
    state :incomplete
    state :not_finish
  end  
end
