class JoinProject < ActiveRecord::Base
  belongs_to :user
  belongs_to :project
  
  validates_uniqueness_of :user_id, :scope => :project_id
  
  include Workflow
  workflow do
    state :requested do
      event :cancel, :transitions_to => :canceled
      event :review, :transitions_to => :in_review
      event :accept, :transitions_to => :accepted
    end
    state :canceled do
      event :request, :transitions_to => :requested
    end
    state :in_review do
      event :accept, :transitions_to => :accepted
      event :reject, :transitions_to => :rejected
    end    
    state :accepted do
      event :reject, :transitions_to => :rejected
      event :abandon, :transitions_to => :abandoned
      event :finish, :transitions_to => :finished
      event :reset_schedule, :transitions_to => :accepted
    end
    state :rejected do
      event :accept, :transitions_to => :accepted
    end
    
    state :abandoned
    state :finished
  end
end
