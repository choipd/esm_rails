class Survey < ActiveRecord::Base
  belongs_to :project
  belongs_to :user
  has_many :questions,:order => 'display_no ASC'
end
