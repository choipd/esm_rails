class Answer < ActiveRecord::Base
  belongs_to :question
  belongs_to :take_survey
  
  validates_uniqueness_of :take_survey_id, :scope => :question_id
end
