class AddTakeSurveyIdToAnswers < ActiveRecord::Migration
  def change
    add_column :answers, :take_survey_id, :integer
  end
end
