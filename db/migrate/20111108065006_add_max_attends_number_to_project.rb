class AddMaxAttendsNumberToProject < ActiveRecord::Migration
  def change
    add_column :projects, :max_attends_number, :integer
  end
end
