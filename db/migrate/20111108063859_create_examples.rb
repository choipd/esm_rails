class CreateExamples < ActiveRecord::Migration
  def change
    create_table :examples do |t|
      t.string :title
      t.references :question

      t.timestamps
    end
    add_index :examples, :question_id
  end
end
