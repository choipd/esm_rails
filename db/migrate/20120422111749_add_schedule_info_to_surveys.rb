class AddScheduleInfoToSurveys < ActiveRecord::Migration
  def change
    add_column :surveys, :begin_at, :time
    add_column :surveys, :end_at, :time
    add_column :surveys, :interval_min, :integer
    add_column :surveys, :interval_max, :integer
  end
end
