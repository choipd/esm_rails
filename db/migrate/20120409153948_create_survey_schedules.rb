class CreateSurveySchedules < ActiveRecord::Migration
  def change
    create_table :survey_schedules do |t|
      t.references :user
      t.references :survey
      t.integer :d
      t.string :t
      t.boolean :attend

      t.timestamps
    end
    add_index :survey_schedules, :user_id
    add_index :survey_schedules, :survey_id
  end
end
