class AddAncestryToDirectories < ActiveRecord::Migration
  def change
    add_column :directories, :ancestry, :string
  end
  def up
    add_index :directories, :ancestry
  end
  def down
    remove_index :directories, :ancestry
  end
end
