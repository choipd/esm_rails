class AddPushSentToSurveySchedule < ActiveRecord::Migration
  def change
    add_column :survey_schedules, :push_sent, :boolean
  end
end
