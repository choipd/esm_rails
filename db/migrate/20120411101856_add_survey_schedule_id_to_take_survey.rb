class AddSurveyScheduleIdToTakeSurvey < ActiveRecord::Migration
  def change
    add_column :take_surveys, :survey_schedule_id, :integer
  end
end
