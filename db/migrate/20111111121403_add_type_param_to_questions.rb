class AddTypeParamToQuestions < ActiveRecord::Migration
  def change
    add_column :questions, :type_param, :string
  end
end
