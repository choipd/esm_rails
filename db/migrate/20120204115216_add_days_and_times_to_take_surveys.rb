class AddDaysAndTimesToTakeSurveys < ActiveRecord::Migration
  def change
    add_column :take_surveys, :days, :integer
    add_column :take_surveys, :times, :integer
  end
end
