class AddWorkflowStateToJoinProject < ActiveRecord::Migration
  def change
    add_column :join_projects, :workflow_state, :string
  end
end
