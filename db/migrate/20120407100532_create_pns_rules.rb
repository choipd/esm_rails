class CreatePnsRules < ActiveRecord::Migration
  def change
    create_table :pns_rules do |t|
      t.integer :start_hour
      t.integer :end_hour
      t.integer :min_time_diff
      t.integer :max_time_diff
      t.integer :pns_count

      t.timestamps
    end
  end
end
