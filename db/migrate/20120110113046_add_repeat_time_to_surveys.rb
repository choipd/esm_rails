class AddRepeatTimeToSurveys < ActiveRecord::Migration
  def change
    add_column :surveys, :repeatTime, :integer
  end
end
