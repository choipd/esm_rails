class CreateDirectories < ActiveRecord::Migration
  def change
    create_table :directories do |t|
      t.string :title
      t.string :description

      t.timestamps
    end
  end
end
