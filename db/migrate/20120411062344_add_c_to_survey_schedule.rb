class AddCToSurveySchedule < ActiveRecord::Migration
  def change
    add_column :survey_schedules, :c, :integer
  end
end
