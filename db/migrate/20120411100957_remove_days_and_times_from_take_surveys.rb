class RemoveDaysAndTimesFromTakeSurveys < ActiveRecord::Migration
  def up
      remove_column :take_surveys, :days
      remove_column :take_surveys, :times
  end

  def down
  end
end
