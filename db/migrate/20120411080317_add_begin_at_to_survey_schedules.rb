class AddBeginAtToSurveySchedules < ActiveRecord::Migration
  def change
    add_column :survey_schedules, :begin_at, :datetime
  end
end
