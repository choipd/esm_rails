class AddWorkflowStateToTakeSurvey < ActiveRecord::Migration
  def change
    add_column :take_surveys, :workflow_state, :string
  end
end
