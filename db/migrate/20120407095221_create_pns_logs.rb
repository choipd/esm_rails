class CreatePnsLogs < ActiveRecord::Migration
  def change
    create_table :pns_logs do |t|
      t.references :user
      t.references :project
      t.references :survey

      t.timestamps
    end
    add_index :pns_logs, :user_id
    add_index :pns_logs, :project_id
    add_index :pns_logs, :survey_id
  end
end
