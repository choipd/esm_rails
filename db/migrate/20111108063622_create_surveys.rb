class CreateSurveys < ActiveRecord::Migration
  def change
    create_table :surveys do |t|
      t.string :title
      t.text :description
      t.references :project

      t.timestamps
    end
    add_index :surveys, :project_id
  end
end
