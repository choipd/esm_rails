class CreateJoinProjects < ActiveRecord::Migration
  def change
    create_table :join_projects do |t|
      t.references :user
      t.references :project

      t.timestamps
    end
    add_index :join_projects, :user_id
    add_index :join_projects, :project_id
  end
end
