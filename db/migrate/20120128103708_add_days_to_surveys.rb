class AddDaysToSurveys < ActiveRecord::Migration
  def change
    add_column :surveys, :days, :integer
  end
end
