class CreateTakeSurveys < ActiveRecord::Migration
  def change
    create_table :take_surveys do |t|
      t.references :user
      t.references :survey

      t.timestamps
    end
    add_index :take_surveys, :user_id
    add_index :take_surveys, :survey_id
  end
end
