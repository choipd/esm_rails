class AddDisplayNoToQuestions < ActiveRecord::Migration
  def change
    add_column :questions, :display_no, :integer
  end
end
