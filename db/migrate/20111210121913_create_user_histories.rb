class CreateUserHistories < ActiveRecord::Migration
  def change
    create_table :user_histories do |t|
      t.references :user
      t.string :action
      t.string :params
      t.string :viewlevel

      t.timestamps
    end
    add_index :user_histories, :user_id
  end
end
