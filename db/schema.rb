# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended to check this file into your version control system.

ActiveRecord::Schema.define(:version => 20120501033930) do

  create_table "admins", :force => true do |t|
    t.string   "email",                                 :default => "", :null => false
    t.string   "encrypted_password",     :limit => 128, :default => "", :null => false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",                         :default => 0
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip"
    t.string   "last_sign_in_ip"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "admins", ["email"], :name => "index_admins_on_email", :unique => true
  add_index "admins", ["reset_password_token"], :name => "index_admins_on_reset_password_token", :unique => true

  create_table "answers", :force => true do |t|
    t.integer  "question_id"
    t.integer  "example_id"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "user_answer"
    t.integer  "take_survey_id"
  end

  add_index "answers", ["example_id"], :name => "index_answers_on_example_id"
  add_index "answers", ["question_id"], :name => "index_answers_on_question_id"

  create_table "device_tokens", :force => true do |t|
    t.integer  "user_id"
    t.string   "token"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "device_tokens", ["user_id"], :name => "index_device_tokens_on_user_id"

  create_table "directories", :force => true do |t|
    t.string   "title"
    t.string   "description"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "ancestry"
  end

  create_table "examples", :force => true do |t|
    t.string   "title"
    t.integer  "question_id"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "user_id"
    t.integer  "display_no"
  end

  add_index "examples", ["question_id"], :name => "index_examples_on_question_id"

  create_table "join_projects", :force => true do |t|
    t.integer  "user_id"
    t.integer  "project_id"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "workflow_state"
  end

  add_index "join_projects", ["project_id"], :name => "index_join_projects_on_project_id"
  add_index "join_projects", ["user_id"], :name => "index_join_projects_on_user_id"

  create_table "pns_logs", :force => true do |t|
    t.integer  "user_id"
    t.integer  "project_id"
    t.integer  "survey_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "pns_logs", ["project_id"], :name => "index_pns_logs_on_project_id"
  add_index "pns_logs", ["survey_id"], :name => "index_pns_logs_on_survey_id"
  add_index "pns_logs", ["user_id"], :name => "index_pns_logs_on_user_id"

  create_table "pns_rules", :force => true do |t|
    t.integer  "start_hour"
    t.integer  "end_hour"
    t.integer  "min_time_diff"
    t.integer  "max_time_diff"
    t.integer  "pns_count"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "posts", :force => true do |t|
    t.string   "title"
    t.text     "description"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "projects", :force => true do |t|
    t.string   "title"
    t.text     "description"
    t.integer  "user_id"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.date     "start_date"
    t.date     "end_date"
    t.integer  "max_attends_number"
    t.integer  "point"
  end

  add_index "projects", ["user_id"], :name => "index_projects_on_user_id"

  create_table "questions", :force => true do |t|
    t.string   "title"
    t.text     "description"
    t.integer  "survey_id"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "user_id"
    t.integer  "qtype"
    t.string   "type_param"
    t.integer  "display_no"
  end

  add_index "questions", ["survey_id"], :name => "index_questions_on_survey_id"

  create_table "rapns_feedback", :force => true do |t|
    t.string   "device_token", :limit => 64, :null => false
    t.datetime "failed_at",                  :null => false
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "rapns_feedback", ["device_token"], :name => "index_rapns_feedback_on_device_token"

  create_table "rapns_notifications", :force => true do |t|
    t.integer  "badge"
    t.string   "device_token",          :limit => 64,                       :null => false
    t.string   "sound",                               :default => "1.aiff"
    t.string   "alert"
    t.text     "attributes_for_device"
    t.integer  "expiry",                              :default => 86400,    :null => false
    t.boolean  "delivered",                           :default => false,    :null => false
    t.datetime "delivered_at"
    t.boolean  "failed",                              :default => false,    :null => false
    t.datetime "failed_at"
    t.integer  "error_code"
    t.string   "error_description"
    t.datetime "deliver_after"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "rapns_notifications", ["delivered", "failed", "deliver_after"], :name => "index_rapns_notifications_on_delivered_failed_deliver_after"

  create_table "survey_schedules", :force => true do |t|
    t.integer  "user_id"
    t.integer  "survey_id"
    t.integer  "d"
    t.string   "t"
    t.boolean  "attend"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "c"
    t.datetime "begin_at"
    t.boolean  "push_sent"
  end

  add_index "survey_schedules", ["survey_id"], :name => "index_survey_schedules_on_survey_id"
  add_index "survey_schedules", ["user_id"], :name => "index_survey_schedules_on_user_id"

  create_table "surveys", :force => true do |t|
    t.string   "title"
    t.text     "description"
    t.integer  "project_id"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "user_id"
    t.integer  "repeatTime"
    t.integer  "days"
    t.time     "begin_at"
    t.time     "end_at"
    t.integer  "interval_min"
    t.integer  "interval_max"
  end

  add_index "surveys", ["project_id"], :name => "index_surveys_on_project_id"

  create_table "take_surveys", :force => true do |t|
    t.integer  "user_id"
    t.integer  "survey_id"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "workflow_state"
    t.integer  "survey_schedule_id"
  end

  add_index "take_surveys", ["survey_id"], :name => "index_take_surveys_on_survey_id"
  add_index "take_surveys", ["user_id"], :name => "index_take_surveys_on_user_id"

  create_table "user_histories", :force => true do |t|
    t.integer  "user_id"
    t.string   "action"
    t.string   "params"
    t.string   "viewlevel"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "user_histories", ["user_id"], :name => "index_user_histories_on_user_id"

  create_table "users", :force => true do |t|
    t.string   "email",                                 :default => "", :null => false
    t.string   "encrypted_password",     :limit => 128, :default => "", :null => false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",                         :default => 0
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip"
    t.string   "last_sign_in_ip"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "name"
    t.string   "token_authenticatable"
    t.string   "authentication_token"
  end

  add_index "users", ["authentication_token"], :name => "index_users_on_authentication_token", :unique => true
  add_index "users", ["email"], :name => "index_users_on_email", :unique => true
  add_index "users", ["reset_password_token"], :name => "index_users_on_reset_password_token", :unique => true

end
